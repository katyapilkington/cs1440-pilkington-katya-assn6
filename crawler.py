import sys
from urllib.parse import urlparse, urldefrag
import requests
from bs4 import BeautifulSoup


""" The beginningCrawl function is meant to be the intro into the recursive function (where the depth starts at 1,
and the visitedSet is made in the function. The url must be an absolute url. The crawl mechanism prints out all links
found on the url page, but only calls crawl() on the links that are web pages (html, php). The beginningCrawl() function
returns a list containing how many links were found and how many links the crawl() function was called on.
"""


def beginningCrawl(url, maxdepth):
    print(url)
    visitedList = {url}
    newURLs = findLinks(url)
    crawlURLs = []
    crawled = {url}
    for link in range(0, len(newURLs)):
        if newURLs[link] not in visitedList:
            visitedList.add(newURLs[link])
            if traversable(newURLs[link]):
                crawlURLs.append(newURLs[link])
            else:
                print('\t', newURLs[link])
    for link in crawlURLs:
        crawl(link, 2, maxdepth, visitedList, crawled)
        crawled.add(link)
    return [len(visitedList), len(crawled)]


"""" Similar to the beginningCrawl() function, but also includes the current depth of the url. the crawled parameter
was added (not included in the assignment description) in order to keep track of how many links were actually crawled
through, not just found.
"""


def crawl(url, depth, maxdepth, visited, crawled):
    indent = "\t" * depth
    print(indent + url)
    if depth >= maxdepth:
        return
    newURLs = findLinks(url)
    crawlURLs = []
    for link in range(0, len(newURLs)):
        if newURLs[link] not in visited:
            visited.add(newURLs[link])
            if traversable(newURLs[link]):
                crawlURLs.append(newURLs[link])
            else:
                print(indent, '\t', newURLs[link])
    for link in crawlURLs:
        crawl(link, depth + 1, maxdepth, visited, crawled)
        crawled.add(link)


""" findLinks takes an absolute url and requests the page, using BeautifulSoup to find all the links available on the
web page. Uses exception handling to ignore urls that cannot be successfully requested. If the href is not present, or
is/contains a fragment url, the fragment is ignored. The new link is then made an absolute url before appending it
to a list. 
"""


def findLinks(url):
    try:
        urlRequest = requests.get(url)
    except ConnectionError:
        return []
    except requests.exceptions.RequestException:
        return []

    newLinks = []
    soup = BeautifulSoup(urlRequest.text, 'html.parser')
    for link in soup.find_all('a'):
        newUrl = link.get('href')
        if newUrl is None:
            continue
        if newUrl.startswith('#'):
            continue
        if newUrl.find('#') != -1:
            newUrl = newUrl.split('#')[0]
        newLinks.append(checkAbsoluteURL(url, newUrl))
    return newLinks


""" checkAbsoluteURL() sees if a new url is an absolute url. If not, it takes the scheme and host name from the parent
url (where the new url was found) and adds it to the new url. 
"""


def checkAbsoluteURL(parentURL, newURL):
    parsedParent = urlparse(parentURL)
    parsedNew = urlparse(newURL)
    absoluteNewURL = ''
    if parsedNew[0] == '':
        absoluteNewURL = parsedParent[0] + '://'
    if parsedNew[1] == '':
        absoluteNewURL = absoluteNewURL + parsedParent[1]
    absoluteNewURL = absoluteNewURL + newURL
    return urldefrag(absoluteNewURL)[0]


""" traversable() checks to see if the url is an appropriate web page to call crawl() on. Returns a boolean value. 
Currently only accepts urls that end with ".php", ".html" and "/". 
"""


def traversable(url):
    if url.find('?') != -1:
        return False
    formats = ['.php', '.html', '/']
    for value in formats:
        if url.endswith(value):
            return True
    else:
        return False


""" The main() function checks for command line arguments (proper calling of the program is python crawler.php 
absolute_url [maxdepth=3]), runs beginningCrawler(), and prints how many links were visited and crawled through.
"""

def main():
    if len(sys.argv) < 2:
        raise NotImplementedError("No URL supplied. Please supply a starting absolute URL as the first argument")

    url = sys.argv[1]

    if urlparse(url)[0] == '' or urlparse(url)[1] == '':
        raise NotImplementedError("Invalid URL supplied. Please supply a starting absolute URL as the first argument")

    if len(sys.argv) < 3:
        maxdepth = 3
    else:
        maxdepth = int(sys.argv[2])

    print("Crawling from " + url + " to maximum depth of ", maxdepth, " links.")
    visited = beginningCrawl(url, maxdepth)

    print(visited[0], " links found, ", visited[1], "links crawled.")


main()
